-- MySQL dump 10.13  Distrib 8.0.29, for Linux (x86_64)
--
-- Host: localhost    Database: tickets
-- ------------------------------------------------------
-- Server version	8.0.29-0ubuntu0.22.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` enum('dev','admin') NOT NULL DEFAULT 'dev',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'admin'),(2,'dev');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` enum('toResolve','resolved','refused','canceled') NOT NULL DEFAULT 'toResolve',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status` DISABLE KEYS */;
INSERT INTO `status` VALUES (1,'toResolve'),(2,'resolved'),(3,'refused'),(4,'canceled');
/*!40000 ALTER TABLE `status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket`
--

DROP TABLE IF EXISTS `ticket`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ticket` (
  `id` int NOT NULL AUTO_INCREMENT,
  `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `createdById` int DEFAULT NULL,
  `assignedToId` int DEFAULT NULL,
  `statusId` int DEFAULT NULL,
  `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `assignedAt` datetime DEFAULT NULL,
  `resolvedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_cdd21a6b9c9d8ccb0de1c695e7e` (`createdById`),
  KEY `FK_fc73d71fe3965e8bf6ff9560c10` (`assignedToId`),
  KEY `FK_7312ac8aab89dd3586729d97ea0` (`statusId`),
  CONSTRAINT `FK_7312ac8aab89dd3586729d97ea0` FOREIGN KEY (`statusId`) REFERENCES `status` (`id`),
  CONSTRAINT `FK_cdd21a6b9c9d8ccb0de1c695e7e` FOREIGN KEY (`createdById`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_fc73d71fe3965e8bf6ff9560c10` FOREIGN KEY (`assignedToId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket`
--

LOCK TABLES `ticket` WRITE;
/*!40000 ALTER TABLE `ticket` DISABLE KEYS */;
INSERT INTO `ticket` VALUES (61,'2022-05-19 20:27:46.462371',2,5,1,'2022-05-19 22:40:52.000000','2022-05-19 20:28:35',NULL),(63,'2022-05-19 22:41:16.641047',2,6,1,'2022-05-20 00:59:53.000000','2022-05-19 22:41:17','2022-05-19 23:39:52'),(64,'2022-05-19 23:39:01.136607',2,6,2,'2022-05-19 23:39:48.000000','2022-05-19 23:39:01','2022-05-19 23:39:49'),(65,'2022-05-20 00:55:34.070031',2,16,2,'2022-05-20 00:56:17.000000','2022-05-20 00:55:34','2022-05-20 00:56:17');
/*!40000 ALTER TABLE `ticket` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updatedAt` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `roleId` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_78a916df40e02a9deb1c4b75ed` (`username`),
  UNIQUE KEY `IDX_e12875dfb3b1d92d7d7c5377e2` (`email`),
  KEY `FK_c28e52f758e7bbc53828db92194` (`roleId`),
  CONSTRAINT `FK_c28e52f758e7bbc53828db92194` FOREIGN KEY (`roleId`) REFERENCES `role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (2,'admin','admin','admin@admin.cl','$2a$08$LmFsDrcyL6uFu91jKFrRlezmEfIDk39yrn6tuvUuYeguZTcWpEfzW','2022-05-18 14:01:20.923079','2022-05-18 14:01:20.923079',1),(3,'dev1','dev1','dev1@dev1.cl','$2a$08$UcUXgb4S/XGNihOYSW.0EeHgln.FDAAgmsTULY6S29Hd5KCF2HUJm','2022-05-18 14:01:54.378125','2022-05-18 14:01:54.378125',2),(5,'dev2','dev2','dev2@dev2.cl','$2a$08$W0JSLedjfq80v3VPDYWv8uNY5BX3AMO65m5wenEGjDeF3xYQSCOCW','2022-05-18 14:02:11.150348','2022-05-18 14:02:11.150348',2),(6,'dev3','dev3','dev3@dev3.cl','$2a$08$dqvVHy09ZrPAhyWj/TL9MuhQnmrVRcitKAvG3J8ZxiLPDnLZv6/Xa','2022-05-18 14:02:23.414929','2022-05-18 14:02:23.414929',2),(16,'dev4','dev4','dev4@dev4.cl','$2a$08$yyC.C3tjAmzf2uXjA1i0fOPu3QwVRej7T46SgGSqpUtARlWP5uCPa','2022-05-20 00:55:09.640469','2022-05-20 00:55:09.640469',2);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'tickets'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-20 12:05:43
