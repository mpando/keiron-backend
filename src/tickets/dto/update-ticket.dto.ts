import { PartialType } from '@nestjs/mapped-types';
import { CreateTicketDto } from './create-ticket.dto';
import { IsEmail, IsNotEmpty, IsNumber, IsDate } from 'class-validator';
import { Status } from '../../status/entities/status.entity';
import { User } from '../../users/entities/user.entity';
import { Role } from 'src/roles/entities/role.entity';

export class UpdateTicketDto extends PartialType(CreateTicketDto) {
  @IsNumber()
  createdBy: User;

  @IsNumber()
  assignedTo: User;

  @IsNumber()
  status: Status;

  @IsDate()
  createdAt: Date;

  @IsDate()
  updatedAt: Date;

  @IsDate()
  assignedAt: Date;

  @IsDate()
  resolvedAt: Date;
}
