import { Injectable } from '@nestjs/common';
import { User } from 'src/users/entities/user.entity';
import { CreateTicketDto } from './dto/create-ticket.dto';
import { UpdateTicketDto } from './dto/update-ticket.dto';
import { Ticket } from './entities/ticket.entity';

interface TicketUser extends User {
  userId: string;
}

@Injectable()
export class TicketsService {
  async create(createTicketDto: CreateTicketDto, user: TicketUser) {
    const ticket = Ticket.create(createTicketDto);
    ticket.createdBy = await User.findOne(user.userId); // JWT current user
    await ticket.save();

    return ticket;
  }

  async findById(id: number) {
    return await Ticket.findOne(id);
  }

  async findAll(user: TicketUser) {
    const userFound = await User.findOne(user.userId, { relations: ['role'] }); // JWT current user
    let finderOptions = {};
    if (userFound.role.name === 'admin') {
      finderOptions = {
        relations: ['createdBy', 'assignedTo', 'status'],
      };
    }
    if (userFound.role.name === 'dev') {
      finderOptions = {
        where: { assignedTo: userFound },
        relations: ['createdBy', 'assignedTo', 'status'],
      };
    }

    return Ticket.find(finderOptions);
  }

  findOne(id: number) {
    return `This action returns a #${id} ticket`;
  }

  update(id: number, updateTicketDto: UpdateTicketDto) {
    return Ticket.update({ id }, updateTicketDto);
  }

  async remove(id: number) {
    return await Ticket.delete(id);
  }
}
