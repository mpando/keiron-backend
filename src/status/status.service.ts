import { Injectable } from '@nestjs/common';
import { CreateStatusDto } from './dto/create-status.dto';
import { UpdateStatusDto } from './dto/update-status.dto';
import { Status } from './entities/status.entity';

@Injectable()
export class StatusService {
  async create(CreateStatusDto: CreateStatusDto) {
    const status = Status.create(CreateStatusDto);
    await status.save();
    return status;
  }

  async findById(id: number) {
    return await Status.findOne(id);
  }

  async findAll() {
    const statuses = Status.find();
    return statuses;
  }

  findOne(id: number) {
    return `This action returns a #${id} status`;
  }

  update(id: number, updateStatusDto: UpdateStatusDto) {
    return `This action updates a #${id} status`;
  }

  remove(id: number) {
    return `This action removes a #${id} status`;
  }
}
