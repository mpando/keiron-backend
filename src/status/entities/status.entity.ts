import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { Status as Statuses } from './status.enum';

@Entity()
export class Status extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: Statuses,
    default: Statuses.TO_RESOLVE,
  })
  name: Statuses;
}
