export enum Status {
  TO_RESOLVE = 'toResolve',
  RESOLVED = 'resolved',
  REFUSED = 'refused',
  CANCELED = 'canceled',
}
