import { IsEmail, IsNotEmpty, IsString, IsDate } from 'class-validator';
import { Role } from '../../roles/entities/role.entity';

export class CreateUserDto {
  @IsString()
  name: string;

  @IsString()
  username: string;

  @IsEmail()
  email: string;

  @IsDate()
  createdAt: string;

  @IsDate()
  updatedAt: string;

  @IsString()
  role: Role;

  @IsNotEmpty()
  password: string;
}
