import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    logger: ['log', 'debug', 'verbose', 'error'],
  });
  app.enableCors(); // local purposes

  await app.listen(3001);
}
bootstrap();
