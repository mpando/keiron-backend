import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { RolesModule } from './roles/roles.module';
import { TicketsModule } from './tickets/tickets.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StatusModule } from './status/status.module';

const entities = [];

@Module({
  imports: [
    UsersModule,
    RolesModule,
    TicketsModule,
    AuthModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    TypeOrmModule.forRoot({
      type: process.env.DB_TYPE as any,
      host: process.env.DB_HOST,
      port: parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      synchronize: false,
      // entities: [User, Role, Ticket, Status],
      migrationsRun: true,
      logging: true,
      migrationsTableName: 'migrations_TypeORM',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      migrations: ['.src/migrations/**/*{.ts,.js}'],
      cli: {
        migrationsDir: './src/migrations',
      },
    }),
    StatusModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
