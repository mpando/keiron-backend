import { IsEmail, IsNotEmpty, IsString, IsNumber } from 'class-validator';
import { Role } from 'src/roles/entities/role.entity';

export class AuthLoginDto {
  @IsEmail()
  username: string;

  @IsNotEmpty()
  password: string;

  @IsString()
  name: string;

  @IsNumber()
  role: Role;
}
