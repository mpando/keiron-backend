import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

import { Role as Roles } from './role.enum';

@Entity()
export class Role extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'enum',
    enum: Roles,
    default: Roles.DEV,
  })
  name: Roles;
}
