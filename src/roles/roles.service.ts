import { Injectable } from '@nestjs/common';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';

@Injectable()
export class RolesService {
  async create(CreateRoleDto: CreateRoleDto) {
    const role = Role.create(CreateRoleDto);
    await role.save();
    return role;
  }

  async findById(id: number) {
    return await Role.findOne(id);
  }

  async findAll() {
    const roles = Role.find();
    return roles;
  }

  findOne(id: number) {
    return `This action returns a #${id} role`;
  }

  update(id: number, updateRoleDto: UpdateRoleDto) {
    return `This action updates a #${id} role`;
  }

  async remove(id: number) {
    // return await Role.delete(id);
    return `This action removes a #${id} role`;
  }
}
